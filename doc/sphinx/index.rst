.. include:: ../../generated/sphinx_rst/index.rst

.. toctree::
    :caption: About
    :hidden:

    license_information
    contributing
    release_notes

.. toctree::
    :caption: User guide
    :hidden:

    getting_started
    toml_format

.. toctree::
    :caption: Code generators
    :hidden:

    c_generator
    cpp_generator
    html_generator
    vhdl_generator

.. toctree::
    :caption: API reference
    :hidden:

    api_reference/hdl_registers
