Release notes
=============

Release history and changelog for the ``hdl_registers`` project.

.. include:: ../../generated/sphinx_rst/release_notes.rst
