# Instructions here: https://docs.gitlab.com/ee/ci/yaml/
# Linter here: https://gitlab.com/tsfpga/hdl_registers/-/ci/lint
# We mainly use a docker images that are built for tsfpga CI.
# See the "docker" folder in the tsfpga repo for details.

stages:
  - test
  - build_documentation
  - deploy


workflow:
  rules:
    # Run pipeline for scheduled (nightly master runs), for merge requests, and
    # when triggered manually on the web.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_PIPELINE_SOURCE == "web"'
    # Pushes to master (a successful merge request is also a "push") should run so
    # that new website is deployed.
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == "master"'
    # PyPI deploy should be run only when there is a tag. However a $CI_COMMIT_TAG is
    # set only on the $CI_PIPELINE_SOURCE = "push" event (not on merge_request_event
    # or schedule). So allow the pipeline to run for this as well.
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG != null'


default:
  # A running job should be canceled if made redundant by a newer pipeline run.
  interruptible: true
  before_script:
    - echo $CI_COMMIT_TAG
    - echo $CI_COMMIT_BRANCH
    - echo $CI_PIPELINE_SOURCE
    - echo $CI_COMMIT_REF_NAME
    - export PYTHONPATH=$(pwd)/hdl_registers


pytest:
  stage: test
  image: tsfpga/ci_py_sim
  script:
    - git clone --depth 1 --single-branch --branch master https://gitlab.com/tsfpga/tsfpga.git ../tsfpga
    - git clone --depth 1 --single-branch --branch master https://gitlab.com/tsfpga/hdl_modules.git ../hdl_modules
    - python3 -m pytest -v --cov hdl_registers --cov-report xml:generated/python_coverage.xml --cov-report html:generated/python_coverage_html hdl_registers/
  artifacts:
    paths:
      - generated/python_coverage.xml
      - generated/python_coverage_html


build_pypi:
  stage: test
  image: tsfpga/ci_py_sim
  script:
    - git clone --depth 1 --single-branch --branch master https://gitlab.com/tsfpga/tsfpga.git ../tsfpga
    - python3 setup.py sdist
  artifacts:
    paths:
      - dist


build_pages:
  stage: build_documentation
  image: tsfpga/ci_py_sim_sphinx
  # Start as soon as pytest job is done. Even if build_pypi in previous stage is not.
  needs: ["pytest"]
  script:
    - git clone --depth 1 --single-branch --branch master https://gitlab.com/tsfpga/tsfpga.git ../tsfpga
    - python3 tools/build_docs.py
  artifacts:
    paths:
      - generated/sphinx_html


deploy_pypi:
  # Deploy artifacts from the build_pypi job run in the previous stage
  stage: deploy
  image: tsfpga/ci_py_sim
  rules:
    - if: '$CI_COMMIT_TAG != null'
  variables:
    TWINE_USERNAME: __token__
    TWINE_PASSWORD: $PYPI_API_TOKEN
  script:
    - python3 -m pip install twine > /dev/null
    - python3 -m twine upload dist/*


pages:
  # Job name "pages" is magic in gitlab. Will deploy content of the "public" folder to the website.
  # Uses artifacts from the build_pages job run in the previous stage.
  stage: deploy
  # Use a minimal image, so no CI time is wasted fetching a larger image. This step does not need
  # anything special.
  image: alpine
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
  script:
    - mv generated/sphinx_html public
  artifacts:
    paths:
      - public
